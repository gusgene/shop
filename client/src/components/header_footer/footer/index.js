import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faCompass from '@fortawesome/fontawesome-free-solid/faCompass';
import faPhone from '@fortawesome/fontawesome-free-solid/faPhone';
import faClock from '@fortawesome/fontawesome-free-solid/faClock';
import faEnvelope from '@fortawesome/fontawesome-free-solid/faEnvelope';

const Footer = () => {
    return (
        <div className="bck_b_dark">
            <div className="container">
              <div className="logo">
                Waves
              </div>
              <div className="wrapper">
                <div className="left">
                  <h2>Contact information</h2>
                  <div className="business_info">
                    <div className="tag">
                      <FontAwesomeIcon
                        icon={faCompass}
                        className="icon"
                      />
                      <div className="nfo">
                        <div>Address</div>
                        <div>Kramer 1234</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="right">

                </div>

              </div>

            </div>
            
        </div>
    );
};

export default Footer;